---
title: "Quais são os Resultados?"
date: 2022-10-03T19:04:54-03:00
draft: false
---

O resultado estará no impacto da vida de dezenas de milhares de usuários de transporte público urbano sobre trilhos. Em um primeiro estudo, considerando apenas o transporte paulistano, é possível afirmar que mais de 17 milhões de pessoas terão a oportunidade de desfrutar de um deslocamento mais confortável, rápido e eficiente. Os dados obtidos serão direcionados para os usuários de forma que estes possam planejar sua viagem com maior antecedência, evitando momentos nos quais os trens se encontram mais cheios do que o normal, ou evitando ir até o metrô ou trem enquanto eles estiverem operando com velocidade reduzida e maior tempo de parada entre as estações.

1. Menor tempo de deslocamento para os 11.000.000 de usuários diários do METRÔ
2. Menor tempo de deslocamento para os 7.800.000 de usuários diários do CPTM