Você já ouviu falar da Tech Rail System?

Muito provavelmente não, mas tenho certeza de que, se você já usou o transporte público, já se atrasou para algum compromisso devido a velocidade reduzida ou super lotação desse tipo de modal de transporte. Visando resolver essa dor que atormenta dezenas de milhões de brasileiros todos os dias, surgiu a TECH RAIL SYSTEM, uma startup do ramo da mobilidade urbana que utiliza de manipulação de dados já disponíveis em trens e metrôs atuais para proporcionar um sistema de transporte mais eficiente, confortável e rápido para todos os usuários.

Gostou da TECH RAIL SYSTEM? Então conheça mais através desse site:

- Quais são os Objetivos?
- Como Melhorar o Transporte?
- Quais são os Resultados?

