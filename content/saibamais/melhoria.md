---
title: "Como Melhorar o Transporte?"
date: 2021-09-18T23:28:40-03:00
draft: false
---

A melhora que será realizada no transporte público, urbano e sobre trilhos consiste na utilização de três ferramentas:

1. Sistema de Visão Computacional - SVC - a ser utilizado nos trens;
2. Sistema de Visão Computacional - SVC - a ser utilizado nas plataformas;
3. Interface com o Sistema do ATS - Controle de Trens;
4. Informações de localização dos usuários com base em dados de GPS;