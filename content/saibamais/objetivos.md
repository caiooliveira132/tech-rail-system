---
title: "Objetivos"
date: 2022-10-03T18:03:46-03:00
draft: false
---

Com a aplicação das ferramentas e as parcerias com a Companhia do Metropolitano de São Paulo S.A. - Metrô São Paulo, Companhia Paulista de Trens Metropolitanos - CPTM, Secretaria de Estado do Transporte Metropolitano - STM e Grow-Prime Group - Eirile - GWP Brasil buscamos identificar a distribuição de usuários nas plataformas e nos trens. A partir disso será possível informar os usuários do transporte sobre trilhos e obter validações de informações da distribuição de pessoas nos trens e nas plataformas de embarque e desembarque.
