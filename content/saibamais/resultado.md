---
title: "Quais são os Resultados?"
date: 2022-10-03T19:04:54-03:00
draft: false
---

O resultado estará no impacto da vida de dezenas de milhares de usuários de transporte público urbano sobre trilhos. Em um primeiro estudo, considerando apenas o transporte paulistano, é possível afirmar que mais de 17 milhões de pessoas terão a oportunidade de desfrutar de um deslocamento mais confortável, rápido e eficiente.

1. Menor tempo de deslocamento para os 11.000.000 de usuários diários do METRÔ
2. Menor tempo de deslocamento para os 7.800.000 de usuários diários do CPTM