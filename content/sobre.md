---
title: "Sobre"
date: 2022-10-03T17:49:39-03:00
draft: false
---

A TECH RAIL SYSTEM é uma startup que surgiu de uma equipe que busca sanar o problema da mobilidade urbana nos grandes centros urbanos, garantindo maior conforto, liberdade e o direito de ir e vir, garantindo constitucionalmente, para os usuários do sistema de trilhos. A startup de inovação em mobilidade traz para o mercado do transporte urbano sobre trilhos um sistema inovador que vai melhorar a saturação de pessoas no embarque e desembarque de passageiros. A proposta visa implementar indicadores de ocupação dos trens nas plataformas, assim como a previsão de tempo para a chegada dos trens nas estações.

Assim, a TECH RAIL SYSTEM busca investimentos que vejam valor na mobilidade urbana, e o quanto a melhora no cotidiano das pessoas pode refletir em outras áreas, como a produtividade no trabalho e a própria qualidade de vida, já que as pessoas terão mais tempo para se dedicar para atividades como lazer, amigos, família, trabalho, artes, dentre muitas outras que eram anteriormente substituídas pelo tempo que seria gasto no deslocamento diário de casa para o trabalho, escola ou faculdade. Assim, a startup busca impactar a vida de dezenas de milhões de pessoas diariamente.
